﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Models
{
    public class UserPointViewModel
    {
        public string Email { get; set; }
        public int PointsSum { get; set; }
    }
}
