﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Common.Models
{
    public class WordCreateEditModel
    {
        [Required]
        public string Question { get; set; }
        [Required]
        public string Answer { get; set; }
        public string CreatorId { get; set; }
        public int Id { get; set; }

    }
}
