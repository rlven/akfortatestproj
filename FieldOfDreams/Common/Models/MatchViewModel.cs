﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Common.Models
{
    public class MatchViewModel
    {
        public int WordId { get; set; }
        public string Answer { get; set; }
        public string Question { get; set; }
        public string UserAnswer { get; set; }
        public char UserAnswerChar { get; set; }
        public int TryCount { get; set; }
        public string UserAnswerBuff { get; set; }
        public int DefaultCounter { get; set; }
    }
}
