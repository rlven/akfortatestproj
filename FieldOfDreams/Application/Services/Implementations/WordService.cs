﻿using Application.Services.Interfaces;
using Common.Models;
using Domain.Entities;
using Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;

namespace Application.Services.Implementations
{
    public class WordService : IWordService
    {
        private readonly IUnitOfWork uow;
        private readonly UserManager<User> userManager;
        private readonly IUserService userService;
        public WordService(IUnitOfWork uow, UserManager<User> userManager, IUserService userService)
        {
            this.uow = uow;
            this.userManager = userManager;
            this.userService = userService;
        }
        public async Task<Word> CreateWordAsync(WordCreateEditModel word)
        {
            var userClaims = userService.GetUserClaims();
            var currentLogedUserId = userManager.GetUserId(userClaims);
            var newWord = new Word()
            {
                Question = word.Question,
                Answer = word.Answer,
                CreatorId = currentLogedUserId
            };
            await uow.WordRepository.AddAsync(newWord);
            await uow.SaveAsync();
            return newWord;
        }

        public Task<Word> GetWordAsync(int id)
        {
            return uow.WordRepository.GetByIdAsync(id);
        }

        public async Task DeleteWordAsync(int id)
        {
            await uow.WordRepository.DeleteByIdAsync(id);
            await uow.SaveAsync();
        }

        public async Task EditWordAsync(WordCreateEditModel word)
        {
            var currentWord = await uow.WordRepository.GetByIdAsync(word.Id);
            currentWord.Question = word.Question;
            currentWord.Answer = word.Answer;
            uow.WordRepository.Update(currentWord);
            await uow.SaveAsync();
        }

        public IEnumerable<Word> GetAll()
        {
            return uow.WordRepository.All.AsEnumerable();
        }
    }
}
