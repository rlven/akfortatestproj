﻿using System;
using Application.Services.Interfaces;
using Common.Models;
using Domain.Entities;
using Domain.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services.Implementations
{
    public class MatchService : IMatchService
    {
        private readonly IUnitOfWork uow;
        private readonly IUserService userService;
        private readonly UserManager<User> userManager;
        private const int FullWordWishPoint = 3;
        public MatchService(IUnitOfWork uow, IUserService userService, UserManager<User> userManager)
        {
            this.uow = uow;
            this.userService = userService;
            this.userManager = userManager;
        }
        public IEnumerable<Match> GetAllMatches()
        {
            return uow.MatchRepository.All.AsEnumerable();
        }

        public IEnumerable<Match> GetCurrentUserMatches()
        {
            var userClaims = userService.GetUserClaims();
            var currentLogedUserId = userManager.GetUserId(userClaims);
            return uow.MatchRepository.GetUserMathces(currentLogedUserId);
        }

        public IEnumerable<UserPointViewModel> GetUsersWithPoints()
        {
            return uow.MatchRepository.GetUsersWithPoints();
        }

        public async Task<bool> WishWord(MatchViewModel model)
        {
            var userClaims = userService.GetUserClaims();
            var currentLogedUserId = userManager.GetUserId(userClaims);
            bool isWon = model.Answer.Equals(model.UserAnswer);
            var match = new Match()
            {
                UserId = currentLogedUserId,
                WordId = model.WordId,
                IsWon = isWon,
                UserAnswer = model.UserAnswer
            };
            if (isWon)
            {
                match.Points += FullWordWishPoint;
            }
            else
            {
                match.Points -= FullWordWishPoint;
            }

            await uow.MatchRepository.AddAsync(match);
            await uow.SaveAsync();
            return isWon;
        }

        public string WishLetter(MatchViewModel model, string word)
        {
            List<int> indexes = new List<int>();
            for (int i = 0; i < model.Answer.Length; i++)
            {
                if (model.Answer[i].Equals(model.UserAnswerChar))
                {
                    indexes.Add(i);
                }
            }
            StringBuilder sb = new StringBuilder(word);
            foreach (var index in indexes)
            {
                sb[index] = model.UserAnswerChar;
            }

            string result = sb.ToString();
            model.UserAnswerBuff = result;

            return sb.ToString();
        }

        public string PrepareWord(MatchViewModel model)
        {
            string word = string.Empty;
            if (model.DefaultCounter == 1)
            {
                List<char> stars = new List<char>();
                for (int i = 0; i < model.Answer.Length; i++)
                {
                    stars.Add('*');
                }

                word = new string(stars.ToArray());
            }
            else
            {
                word = model.UserAnswerBuff;
            }

            return word;
        }

        public async Task SaveMatchSession(MatchViewModel model, bool isWon, string userAnswer, int wordId)
        {
            var userClaims = userService.GetUserClaims();
            var points = isWon ? +1 : -1;
            var currentLogedUserId = userManager.GetUserId(userClaims);
            var matchSession = new Match()
            {
               UserAnswer = userAnswer, WordId = wordId, IsWon = isWon, UserId = currentLogedUserId, Points = points
            };
            await uow.MatchRepository.AddAsync(matchSession);
            await uow.SaveAsync();
        }
    }
}
