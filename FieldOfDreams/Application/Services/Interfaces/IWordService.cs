﻿using Common.Models;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services.Interfaces
{
    public interface IWordService
    {
        IEnumerable<Word>GetAll();
        Task EditWordAsync(WordCreateEditModel word);
        Task DeleteWordAsync(int id);
        Task<Word> CreateWordAsync(WordCreateEditModel word);
        Task<Word> GetWordAsync(int id);
    }
}
