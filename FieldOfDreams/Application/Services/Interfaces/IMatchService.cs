﻿using Common.Models;
using Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.Services.Interfaces
{
    public interface IMatchService
    {
        IEnumerable<Match> GetAllMatches();
        IEnumerable<Match>GetCurrentUserMatches();
        IEnumerable<UserPointViewModel> GetUsersWithPoints();
        Task<bool> WishWord(MatchViewModel model);
        string WishLetter(MatchViewModel model, string word);
        string PrepareWord(MatchViewModel model);
        Task SaveMatchSession(MatchViewModel model, bool isWon, string userAnswer, int wordId);
    }
}
