﻿using Common.Models;
using Domain.Entities;
using Domain.Interfaces.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace Infrastructure.Repositories
{
    public class MatchRepository : Repository<Match, int>, IMatchRepository
    {
        private readonly AppDbContext context;
        public MatchRepository(AppDbContext context) : base(context)
        {
            this.context = context;
        }

        public IEnumerable<UserPointViewModel> GetUsersWithPoints()
        {
            return context.Matches.GroupBy(x => x.User.Email).Select(z => new UserPointViewModel()
            {
                Email = z.Key,
                PointsSum = z.Sum(q=>q.Points)
            });
        }

        public IEnumerable<Match> GetUserMathces(string userId)
        {
            return context.Matches.Where(x => x.UserId.Equals(userId));
        }
    }
}
