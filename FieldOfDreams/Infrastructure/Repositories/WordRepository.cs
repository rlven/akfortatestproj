﻿using Domain.Entities;
using Domain.Interfaces.Repositories;

namespace Infrastructure.Repositories
{
    public class WordRepository : Repository<Word, int>, IWordRepository
    {
        public WordRepository(AppDbContext context) : base(context)
        {
        }
    }
}
