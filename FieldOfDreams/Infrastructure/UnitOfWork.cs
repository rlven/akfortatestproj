﻿using Domain.Interfaces;
using Domain.Interfaces.Repositories;
using Infrastructure.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext context;
        public IUserRepository userRepository;

        public IWordRepository wordRepository;

        public IMatchRepository matchRepository;
        public UnitOfWork(AppDbContext context)
        {
            this.context = context;
        }
        public IUserRepository UserRepository
        {
            get { return userRepository = new UserRepository(context); }
        }
        public IWordRepository WordRepository
        {
            get { return wordRepository = new WordRepository(context); }
        }
        public IMatchRepository MatchRepository
        {
            get { return matchRepository = new MatchRepository(context); }
        }
        public void Save()
        {
            context.SaveChanges();
        }

        public async Task SaveAsync()
        {
            await context.SaveChangesAsync();
        }

        private bool _disposed;

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
