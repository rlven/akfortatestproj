﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Entities
{
    public class Match : Entity<int>
    {
        [ForeignKey("User")]
        public string UserId { get; set; }
        public virtual User User { get; set; }
        [ForeignKey("Word")]
        public int WordId { get; set; }
        public virtual Word Word { get; set; }
        public bool IsWon { get; set; }
        public int Points { get; set; }
        public string UserAnswer { get; set; }
    }
}
