﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Entities
{
    public class Word : Entity<int>
    {
        public string Question { get; set; }
        public string Answer { get; set; }
        [ForeignKey("Creator")]
        public string CreatorId { get; set; }
        public virtual User Creator { get; set; }
        public virtual ICollection<Match> Matches { get; set; }
    }
}
