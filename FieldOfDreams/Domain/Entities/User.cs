﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class User : IdentityUser
    {
        public virtual ICollection<Word> Words { get; set; }
        public virtual ICollection<Match> Matches { get; set; }
    }
}
