﻿using Common.Models;
using Domain.Entities;
using System.Collections.Generic;

namespace Domain.Interfaces.Repositories
{
    public interface IMatchRepository : IRepository<Match, int>
    {
        IEnumerable<Match>GetUserMathces(string userId);
        IEnumerable<UserPointViewModel> GetUsersWithPoints();
    }
}
