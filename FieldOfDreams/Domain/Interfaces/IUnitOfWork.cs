﻿using Domain.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IUnitOfWork
    {
       IUserRepository UserRepository { get; }
       IWordRepository WordRepository { get; }
       IMatchRepository MatchRepository { get; }

        void Save();
        Task SaveAsync();
    }
}
