﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Services.Interfaces;
using Common.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FieldOfDreams.Controllers
{
    public class MatchController : Controller
    {
        private readonly IMatchService matchService;
        private readonly IWordService wordService;
        public MatchController(IMatchService matchService, IWordService wordService)
        {
            this.matchService = matchService;
            this.wordService = wordService;
        }

        [Authorize(Roles = "Admin")]
        public IActionResult Index()
        {
            var matches = matchService.GetAllMatches();
            return View(matches);
        }

        [Authorize(Roles = "User")]
        public IActionResult GetCurrentUserMatches() 
        {
            var matches = matchService.GetCurrentUserMatches();
            return View(matches);
        }

        [Authorize(Roles = "Admin")]
        public IActionResult GetUsersWithPoints() 
        {
            var usersWithPoints = matchService.GetUsersWithPoints();
            return View(usersWithPoints);
        }

        [Authorize(Roles = "User")]
        public IActionResult GetGameView()
        {
            Random rnd = new Random();
            var word = wordService.GetAll().ToList();
            var model = new MatchViewModel();
            if (word.Any())
            {
                int r = rnd.Next(word.Count());
                model.Question = word[r].Question;
                model.WordId = word[r].Id;
                model.Answer = word[r].Answer;
                return View(model);
            }

            return View("NoGame");
        }

        [Authorize(Roles = "User")]
        [HttpPost]
        public async Task<IActionResult> WishWord(MatchViewModel model)
        {
            var isWon = await matchService.WishWord(model);
            if (isWon)
            {
                return Json(new { isWon, message="Вы угадали!"});
            }
            else
            {
                return Json(new { isWon, message = "Вы проиграли!" });
            }
        }

        [Authorize(Roles = "User")]
        [HttpPost]
        public async Task<IActionResult> WishLetter(MatchViewModel model, int? wordId)
        {
            var isNumeric = char.IsDigit(model.UserAnswerChar);
            var initialWord = matchService.PrepareWord(model);
            if (!isNumeric && !(model.TryCount > model.Answer.Length - 2))
            {
                if (!model.Answer.Contains(model.UserAnswerChar))
                {
                    return Json(new { isWon = false, message = "Не правильно! У вас осталось несколько попыток", isFinish = false, word = initialWord });
                }

                var word = matchService.WishLetter(model, initialWord);
                if (!word.Contains("*"))
                {
                    await matchService.SaveMatchSession(model, true, word, (int)wordId);
                    return Json(new { isWon = true, message = "Вы выиграли!", isFinish = true, word });
                }

                return Json(new {isWon = true, word, answerbuff = model.UserAnswerBuff, message = "Вы отгадали! продолжайте...", isFinish = false });
            }

            await matchService.SaveMatchSession(model, false, model.UserAnswerBuff, (int)wordId);
            return Json(new { isWon = false, message = "Вы проиграли!", isFinish = false });
        }
    }
}