﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Services.Interfaces;
using Castle.Core.Internal;
using Common.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FieldOfDreams.Controllers
{
    [Authorize(Roles = "Admin")]
    public class WordController : Controller
    {
        private readonly IWordService wordService;
        private readonly IUserService userService;
        public WordController(IWordService wordService, IUserService userService)
        {
            this.wordService = wordService;
            this.userService = userService;
        }
        public IActionResult Index()
        {
            var words = wordService.GetAll();
            return View(words);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id==null)
            {
                return NotFound();
            }

            var word = await wordService.GetWordAsync((int)id);
            return View(word);
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var word = await wordService.GetWordAsync((int)id);
            var model = new WordCreateEditModel()
            {
                Answer = word.Answer,
                Question = word.Question,
                CreatorId = word.CreatorId
            };

            ViewBag.Users = new SelectList(userService.GetUsers(), "Id", "Email");

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(WordCreateEditModel model)
        {
            if (ModelState.IsValid)
            {
                await wordService.EditWordAsync(model);
                return RedirectToAction(nameof(Index));
            }

            return View(model);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            await wordService.DeleteWordAsync((int)id);
            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(WordCreateEditModel model)
        {
            if (ModelState.IsValid)
            {
                await wordService.CreateWordAsync(model);
                return RedirectToAction(nameof(Index));
            }

            return View();
        }
    }
}