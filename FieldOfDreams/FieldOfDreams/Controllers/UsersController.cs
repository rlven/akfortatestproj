﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Services.Interfaces;
using Castle.Core.Internal;
using Common.Models;
using Domain.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FieldOfDreams.Controllers
{
    [Authorize(Roles = "Admin")]
    public class UsersController : Controller
    {
        private readonly IUserService userService;
        public UsersController(IUserService userService)
        {
            this.userService = userService;
        }

        public IActionResult Index()
        {
            return View(userService.GetUsers());
        }

        public async Task<IActionResult> Details(string id)
        {
            if (id.IsNullOrEmpty())
            {
                return NotFound();
            }

            var user = await userService.GetUserAsync(id);
            return View(user);
        }

        [HttpGet]
        public async Task<IActionResult> Edit(string id)
        {
            if (id.IsNullOrEmpty())
            {
                return NotFound();
            }

            var user = await userService.GetUserAsync(id);
            var model = new UserCreateEditModel()
            {
                Email = user.Email, UserName = user.UserName, Phone = user.PhoneNumber
            };
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(UserCreateEditModel model)
        {
            if (ModelState.IsValid)
            {
                await userService.EditUserAsync(model);
                return RedirectToAction(nameof(Index));
            }

            return View(model);
        }

        public async Task<IActionResult> Delete(string id)
        {
            if (id.IsNullOrEmpty())
            {
                return NotFound();
            }

            await userService.DeleteUserAsync(id);
            return RedirectToAction(nameof(Index));
        }
    }
}